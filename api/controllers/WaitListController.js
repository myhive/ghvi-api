module.exports = {

    makeWaitList: function (req, res) {
        var waitList = {
            firstName: req.param('firstName'),
            lastName: req.param('lastName'),
            resourceName: req.param('resourceName'),
            resourceId: req.param('resourceId'),
            bedCount: req.param('bedCount'),
            email: req.param('email'),
            phone: req.param('phone')
        }


        WaitList.create(waitList).exec(function (err, newWaitList) {
            // console.log(newWaitList);
            var resourceId = req.param('resourceId');
            console.log(resourceId);
            Resource.findOne(resourceId).exec(function (err, resource) {
                // console.log(resource);
                var bedCount = req.param('bedCount');
                // console.log(bedCount);
                // console.log(resource.servicecapacity_available - bedCount)
                Resource.update(resource.id, { servicecapacity_available: (resource.servicecapacity_available - bedCount) }).exec(function (err, updated) {
                    // console.log(updated);
                    return res.json('Added to waiting list!');
                });
            })
        });
    }

}