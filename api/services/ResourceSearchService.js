//put requires here
var moment = require('moment');
module.exports = {

    findResources: function (req, cb) {
        var cat = req.param('cat'); //category
        var men = req.param('men');
        var women = req.param('women');
        var genderNotSpecified = 0;
        var children = req.param('children');
        var searchLimit = req.param('limit') ? req.param('limit') : 15; //default limit 15
        var page = req.param('page') ? req.param('page') : 1;
        var skipCount = searchLimit * (page - 1);

        // if(men + women + children + genderNotSpecified == 0){
        //     console.log("no applicants found");
        //     cb( new Error("There are no applicants."));
        //     return;
        // }

        //TODO: account for gender not specified persons.
        var cleanParams = {};
        switch (cat) {
            case "shelter": {
                if (children > 0 && men + women + genderNotSpecified == 0) {
                    cleanParams.category = ["emergencyshelter", "indieshelter", "winteronlyshelter", "youthprogram"]
                }
                else {
                    cleanParams.category = { contains: "shelter" }
                }

                break;
            }
            case "longTermAid": {
                cleanParams.category = ["rapidrehousing", "permsupporthousing", "vasystem"]
                if (children > 0 && men + women + genderNotSpecified == 0) {
                    cleanParams.category = ["rapidrehousing", "permsupporthousing", "youthprogram"];
                }
                //TODO: include rehab and recovery programs here.
                break;
            }
            case "emergency": {
                if (children > 0 && (men + women + genderNotSpecified == 0)) {
                    cleanParams.category = ["youthprogram"];
                }
                else {
                    cleanParams.category = ["domesticviolence", { contains: "shelter" }]
                    //TODO: include detox and accute distress aid.
                }
                break;
            }
            case "dailyNeeds": {
                cleanParams.category = ["nonhomelessservices", "vasystem", "transitional"]
                if (children > 0 && men + women + genderNotSpecified == 0) {
                    cleanParams.category = ["nonhomelessservices", "vasystem", "transitional", "youthprogram"];
                }
                //TODO: include "get your meds" and such.
                break;
            }
        };
        if (men + women > 0 && children > 0) {
            cleanParams.servefamilies = "yes";
        }
        // else if(men > 0 && women > 0){
        //     //Logic for non-exclusion should go here.
        // }
        else {
            if (men > 0) {
                cleanParams.servemengt18 = "yes";
            }
            if (women > 0) {
                cleanParams.servesinglewomengt18 = "yes"
            }
        }



        Resource.find({ where: cleanParams, skip: skipCount, limit: searchLimit }).sort("rating desc").exec(function (err, resources) {
            cb(resources);
        })





    }

}