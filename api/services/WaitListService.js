// load aws sdk
var aws = require('aws-sdk');

// load aws config
aws.config.loadFromPath('awsconfig.json');

// load AWS SES
var ses = new aws.SES({ apiVersion: '2010-12-01' });
ses.config.update({ region: 'us-east-1' });

var plivo = require('plivo');
var p = plivo.RestAPI({
    authId: 'MANJLKNMZLMMM0ZMEWM2',
    authToken: 'MjA1NzY2NWVlYjU5NTI2YzE2M2IxN2EyNmIwOTcz'
});

module.exports = {

    messageUser: function (finalCB) {
        WaitList.find({
            where: { processed: null },
            limit: 10,
            sort: 'createdAt DESC'
        }).exec(function (err, waitLists) {
            if (waitLists && waitLists.length > 0) {
                _.forEach(waitLists, function (waitList) {

                    if (waitList.phone) {

                        waitList.processed = true;
                        Resource.findOne(waitList.resourceId).exec(function (err, resource) {

                            var messageText = 'Operation Citizen - ' + waitList.resourceName + ' is available! Phone: ' + resource.contactp1 + ' email: ' + resource.contacte1;
                            var message = {
                                to: waitList.phone,
                                text: messageText,
                                outbound: true
                            }
                            console.log('inbound called' + JSON.stringify(message));


                            Message.create(message).exec(function (err, messages) {
                                var params = {
                                    'src': '18053650050',
                                    'dst': message.to.replace(/[^0-9]/, ''),
                                    'text': message.text
                                };
                                p.send_message(params, function (status, response) {
                                    console.log('Status: ', status);
                                    console.log('API Response:\n', response);
                                    if (!waitList.email) {
                                        waitList.save(function () {
                                            console.log('sms sent: Saved');
                                            finalCB();
                                        });
                                    }
                                });

                            });
                        });
                    }

                    if (waitList.email) {
                        // send to list
                        var to = [waitList.email]

                        // this must relate to a verified SES account
                        var from = 'chad@myhive.io'

                        // this sends the email
                        // @todo - add HTML version
                        
                        Resource.findOne(waitList.resourceId).exec(function (err, resource) {

                            ses.sendEmail({
                                Source: from,
                                Destination: { ToAddresses: to },
                                Message: {
                                    Subject: {
                                        Data: 'Operation Citizen - ' + waitList.resourceName + ' is available'
                                    },
                                    Body: {
                                        Text: {
                                            Data: 'The resource you requested is now available - ' + waitList.resourceName + '.  Click here to view the rousource. https://operation-citizen.firebaseapp.com/view4/?resourceid=' + waitList.resourceId +
                                            ', or contact by phone at ' + resource.contactp1 + ' or email at' + resource.contacte1 + '',
                                        }
                                    }
                                }
                            }, function (err, data) {
                                if (err) {
                                    console.log('Email sent:' + err);
                                    waitList.processed = true;
                                    waitList.save(function () {
                                        console.log('Email not sent: Saved');
                                        finalCB();
                                    });
                                }
                                console.log('Email sent:');
                                console.log(data);
                                // update waitList 
                                waitList.notificationSent = true;
                                waitList.processed = true;

                                waitList.save(function () {
                                    console.log('Email sent: Saved');
                                    finalCB();
                                });
                            });
                        });
                    }

                    // nothing to process
                    if (!waitList.email && !waitList.phone) {
                        waitList.processed = true;
                        waitList.save(function () {
                            console.log('Email sent: Saved');
                            finalCB();
                        });
                    }

                });
            } else {
                finalCB();
            }

        });
    }

};