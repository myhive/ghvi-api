var running = false;
module.exports.cron = {
  waitlistJob: {
    schedule: '*/30 * * * * *',
    onTick: function () {
      console.log('calling  WaitListService.messageUser');
      if (running == false) {
        running = true;
        WaitListService.messageUser(function () {
          running = false;
          console.log('done calling  WaitListService.messageUser');
        });
      }
    }
  },
  //   ipJob: {
  //     schedule: '* 1 * * * *',
  //     onTick: function () {
  //         console.log('calling  WaitListService.messageUser');
  //       WaitListService.messageUser(function(){
  //         console.log('done calling  WaitListService.messageUser');

  //       });
  //     }
  //   }
};